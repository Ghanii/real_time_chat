<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Styles -->
        <style>
            .nav.nav-tabs{
          border : none ;
          color: rgb(99, 127, 156) ;
      }
      
      
      
      ul li a{
          color: rgb(99, 127, 156);
          border-bottom: 3px solid rgb(99, 127, 156);
      }
      ul li a:hover{
          color:  rgb(52, 93, 136);
          border: 1px solid rebeccapurple;
      
      }
      
      .nav.nav-tabs .nav-item .active {
          border: none;
          background-color: transparent;
          border-bottom: 3px solid rgb(99, 127, 156);
          color: rgb(99, 127, 156); ;
      }
      
            /* html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            } */
        </style>
    </head>
    <body>
      
        <div id="chat">
          

           <chate :user={{ Auth::user() }}> </chate>
        </div>

        <script>
            console.log('hello world');
            
        </script>
        
        
    </body>
</html>
