<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// Broadcast::channel('message', function ($user) {
//     Log::info(json_encode($user));
//     //return $user->verified ;
//     if($user->verified){
//         return $user->toArray() ;
//     }
// });

Broadcast::channel('chat', function ($user) {
    Log::info($user);
    return true ;

});


Broadcast::channel('test', function ($user) {
    return $user->verified ;

});