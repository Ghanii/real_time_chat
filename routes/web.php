<?php

use App\Events\chat;
use App\Events\message;
use App\Http\Controllers\chat as ControllersChat;
use App\Http\Controllers\TestCtrl;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    event(new message('beyonce love of my life')) ;
    return view('welcome');

});

//Route::get('/', 'HomeController@test');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tst' ,'TestCtrl@index' );
Route::get('/test' , function (){
    return response()->json(['name' => "abdelhamid"]) ;
});

Route::get('/chat' , function(){
    return view('chat') ;
});

Route::post('/chat' , 'chat@broadCastMssg')->middleware('auth');

Route::get('/users' , 'chat@users')->middleware('auth');

