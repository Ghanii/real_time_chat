<?php

namespace App\Http\Controllers;

use App\Events\chat;
use Illuminate\Http\Request;

class TestCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        event( new chat('hi') ) ;
        return view('test') ;
    }
}
