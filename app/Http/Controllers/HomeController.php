<?php

namespace App\Http\Controllers;

use App\Events\message;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        event(new message('authenticated'));
        return view('home');
    }

    /*public function test(){
        event(new message('authenticated'));
        return view('welcome');
    }*/
}
